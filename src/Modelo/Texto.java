/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JOptionPane;
import ufps.util.colecciones_seed.TablaHash;

/**
 *
 * @author Richard Acevedo
 */
public class Texto {

    private TablaHash<String, String> tabla;

    public Texto() {
        this.tabla = new TablaHash<>(23);
        llenarTablaHash();
    }

    public TablaHash<String, String> getTabla() {
        return tabla;
    }

    private void llenarTablaHash() {
        String arreglo[] = {"a", "b", "c", "ch", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "\\\\n", " "};

        String valor[] = {"000001", "000010", "000011", "000100", "000101", "000110", "000111", "001000", "001001",
            "001010", "001011", "001100", "001101", "001110", "001111", "110000", "110001", "110010", "110011",
            "110100", "110101", "110110", "110111", "111000", "111001", "111010", "111011", "111100", "111101", "111110"};
        for (int i = 0; i < valor.length; i++) {
            this.tabla.insertar(arreglo[i], valor[i]);
        }

    }

    public String leerArchivo(String ruta) throws IOException {
        String textoComp = "";
        try {
            FileReader entrada = new FileReader(ruta);
            BufferedReader leer = new BufferedReader(entrada);
            String letra = "";
            while (letra != null) {
                letra = leer.readLine();
                if (letra != null) {
                    textoComp += letra;
                }
            }
        } catch (IOException e) {
            System.out.println("errr");
            e.printStackTrace();
        }
        return textoComp;
    }

    public String[][] convertirMatriz(String txt) {
        String[][] matriz = null;
        String[] lineas = txt.split("\\\\n");
        int tam = lineas[0].length();
        //TAM ES EL NUMERO MAXIMO DE COLUMNAS
        for (int i = 0; i < lineas.length; i++) {
            for (int j = 0; j < lineas[i].length(); j++) {
                if (lineas[i].length() > tam) {
                    //SE COMPARA Y SE ENCUENTRA EL MAYOR
                    tam = lineas[i].length();
                }
            }
        }
        matriz = new String[lineas.length][tam];
        for (int m = 0; m < lineas.length; m++) {
           int indice=0;
            for (int n = 0; n < lineas[m].length(); n++,indice++) {

                    if(lineas[m].charAt(n) == '0' || lineas[m].charAt(n) == '1' || lineas[m].charAt(n) == '2' || lineas[m].charAt(n) == '3' || lineas[m].charAt(n) == '4' || lineas[m].charAt(n) == '5' || lineas[m].charAt(n) == '6' || lineas[m].charAt(n) == '7' || lineas[m].charAt(n) == '8' || lineas[m].charAt(n) == '9'){
                        JOptionPane.showMessageDialog(null, "Se encontro un valor numero no permitido ( "+lineas[m].charAt(n)+" ) en la posicion ( "+m+" - "+n+" ), el cual sera omitido inmediatamente");
                        continue;
                        
                    }
                    String letra = String.valueOf(lineas[m].charAt(n));
                    matriz[m][n] = letra;
                
            }
        }
        return matriz;
    }

}
