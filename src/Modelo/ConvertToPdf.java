package Modelo;

import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import javax.swing.*;


public class ConvertToPdf {

    public static void printFrameToPDF(JFrame componente) {
        String nombreArchivo = "arbol-binario.pdf";

        try {

            com.lowagie.text.Rectangle tamanio = PageSize.LETTER.rotate();

            Document document = new Document(tamanio);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(nombreArchivo));
            document.open();

            document.addAuthor("Pues yo");
            document.addCreator("openpdf-1.2.16");
            document.addTitle("Arbol Digital");
            document.addCreationDate();
            document.addSubject("Exportacion de arbol digital");

            PdfContentByte cb = writer.getDirectContent();
            PdfTemplate template = cb.createTemplate(tamanio.getWidth(), tamanio.getHeight());
            cb.addTemplate(template, 0, 0);

            document.add(new Paragraph("Examen Final -  Analisis de Algoritmos - Arbol Digital - Programa Elaborado por Richard Acevedo y Manuel Coronel - 2019-2"));
            Graphics2D g2d = template.createGraphics(tamanio.getWidth(), tamanio.getHeight());
            g2d.scale(0.54 ,0.9);
            g2d.translate(0, 100);

            for (int i = 0; i < componente.getContentPane().getComponents().length; i++) {
                Component c = componente.getContentPane().getComponent(i);
                if (c instanceof JScrollPane) {
                    c.setBounds(0, 0, (int) tamanio.getWidth() * 2, (int) tamanio.getHeight() * 2);
                    c.paintAll(g2d);
                    c.addNotify();
                }
            }

            g2d.dispose();

            document.close();
            writer.close();

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Guardar archivo en ...");
            fileChooser.setSelectedFile(new File("arbol-binario.pdf"));

            int userSelection = fileChooser.showSaveDialog(componente);

            if (userSelection == JFileChooser.APPROVE_OPTION) {
                File fileToSave = fileChooser.getSelectedFile();

                File file = new File(nombreArchivo);

                try {
                    Files.copy(file.toPath(), fileToSave.toPath());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (Exception e) {
            System.out.println("ERROR: " + e.toString());
        }
    }
}
