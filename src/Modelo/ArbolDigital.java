package Modelo;

import java.io.IOException;
import java.util.Iterator;
import ufps.util.colecciones_seed.ArbolBinario;
import ufps.util.colecciones_seed.ArbolBinarioBusqueda;
import ufps.util.colecciones_seed.Cola;
import ufps.util.colecciones_seed.NodoBin;

public class ArbolDigital extends ArbolBinario {

    Texto tex = new Texto();

    public ArbolDigital(String url) throws IOException {
        super("X");
        String texto = tex.leerArchivo(url);
        String[][] matriz = tex.convertirMatriz(texto);
        this.crearArbolDigital(matriz);
    }

    public void crearArbolDigital(String[][] x) {

        for (int i = 0; i < x.length; i++) {
            int indice = 0;
            for (int j = 0; j < x[0].length; j++, indice++) {
                if (x[i][j] != null) {
                    NodoBin<Object> l = this.getRaiz();
                    if (x[i][j].equals("c")) {

                        if (j + 1 < x[i].length) {
                            if (x[i][j + 1].equals("h")) {
                                this.ingresarBinarioRecursivo(0, tex.getTabla().getObjeto("ch").toString(), new Coordenadas(i, indice), l, x[i][j]);
                                j++;

                            } else {
                                this.ingresarBinarioRecursivo(0, tex.getTabla().getObjeto("c").toString(), new Coordenadas(i, indice), l, x[i][j]);

                            }
                        } else {

                            this.ingresarBinarioRecursivo(0, tex.getTabla().getObjeto("c").toString(), new Coordenadas(i, indice), l, x[i][j]);
                        }
                    } else {
                        this.ingresarBinarioRecursivo(0, tex.getTabla().getObjeto(x[i][j]).toString(), new Coordenadas(i, indice), l, x[i][j]);
                    }
                }
            }
        }
    }

    public Cola<Coordenadas> getHojaCoordenasIterativo(String binario) {
        Cola<Coordenadas> cola = new Cola();
        NodoBin<Object> l = this.getRaiz();
        for (int i = 0; i < binario.length(); i++) {

            if (binario.charAt(i) == '0') {
                if (l.getIzq() != null) {
                    l = l.getIzq();
                } else {

                    return null;
                }
            } else {
                if (l.getDer() != null) {

                    l = l.getDer();
                } else {

                    return null;

                }
            }
        }

        cola = (Cola) l.getIzq().getInfo();

        return cola.clonar();
    }

    public void ingresarBinarioRecursivo(int indice, String binario, Coordenadas posicion, NodoBin l, String letra) {
        if (indice == 6) {
            agregarCoordenadas(l, posicion, letra);
            return;
        }
        if (binario.charAt(indice) == '0') {
            if (l.getIzq() == null) {
                NodoBin tem = new NodoBin("0");
                l.setIzq(tem);

            }
            l = l.getIzq();
        } else {
            if (l.getDer() == null) {
                NodoBin tem = new NodoBin("1");
                l.setDer(tem);
            }
            l = l.getDer();
        }

        ingresarBinarioRecursivo(indice + 1, binario, posicion, l, letra);

    }

    public void ingresarBinarioIterativo(String binario, Coordenadas posicion) {
        NodoBin<Object> l = this.getRaiz();
        for (int i = 0; i < binario.length(); i++) {
            if (binario.charAt(i) == '0') {
                if (l.getIzq() == null) {
                    l.setIzq(new NodoBin("0"));
                }
                l = l.getIzq();
            } else {
                if (l.getDer() == null) {
                    l.setDer(new NodoBin("1"));
                }
                l = l.getDer();
            }
        }
        agregarCoordenadas(l, posicion, "");
    }

    public void agregarCoordenadas(NodoBin<Object> l, Coordenadas posicion, String letra) {
        if (l.getIzq() == null) {
            Cola<Coordenadas> p = new Cola<>();
            p.enColar(posicion);
            l.setIzq(new NodoBin(p));
        } else {
            Cola<Coordenadas> k = (Cola) (l.getIzq().getInfo());
            k.enColar(posicion);
        }
    }

//GENERA UNA COLA DONDE TIENE EN CUENTA LOS ESPCACIOS YA ENCONTRADOS Y LE AGREGA 
    //Y LOS ESPACIOS AL INICIAR CADA CADA RENGLON
    public Cola crearColaConEspacioIncioRenglon(Cola<Coordenadas> espaciosBlancos) {

        Cola<Coordenadas> espaciosBlancosNuevos = new Cola();
        espaciosBlancosNuevos.enColar(new Coordenadas(0, -1));
        if (espaciosBlancos == null) {
            return espaciosBlancosNuevos;
        }
        int fila = espaciosBlancos.getInfoInicio().getX();
        while (espaciosBlancos.getTamanio() > 0) {

            if (espaciosBlancos.getInfoInicio().getX() != fila) {

                espaciosBlancosNuevos.enColar(new Coordenadas(espaciosBlancos.getInfoInicio().getX(), -1));
                fila = espaciosBlancos.getInfoInicio().getX();
            }
            espaciosBlancosNuevos.enColar(espaciosBlancos.deColar());

        }

        return espaciosBlancosNuevos;
    }

    public Cola[] getVectorColaCoordenanadas(String cadena) {

        Cola<Coordenadas> vecCola[];

        Cola<Coordenadas> espaciosBlancos = new Cola();

        espaciosBlancos = crearColaConEspacioIncioRenglon((Cola) (this.getHojaCoordenasIterativo(tex.getTabla().getObjeto(" ").toString())));

        vecCola = new Cola[cadena.length() - contarCantidadDeCh(cadena)];
        vecCola[0] = espaciosBlancos;
        boolean ch = false;
        int indice = 1;
        for (int i = 1; i < cadena.length(); i++, indice++) {

            Cola p = null;
            if (cadena.charAt(i) == 'c') {
                if (i + 1 < cadena.length()) {
                    if (cadena.charAt(i + 1) == 'h') {
                        p = this.getHojaCoordenasIterativo(tex.getTabla().getObjeto("ch").toString());
                        ch = true;
                        i++;
                    } else {
                        p = this.getHojaCoordenasIterativo(tex.getTabla().getObjeto("c").toString());
                    }

                } else {
                    p = this.getHojaCoordenasIterativo(tex.getTabla().getObjeto("c").toString());
                }

            } else {
                p = this.getHojaCoordenasIterativo(tex.getTabla().getObjeto(cadena.charAt(i) + "").toString());
            }

            vecCola[indice] = p;

        }
        if (espaciosBlancos.getTamanio() == 1) {
            vecCola[vecCola.length - 1] = espaciosBlancos.clonar();
        }
        return vecCola;
    }

    //VALIDACION QUE AL FINAL DE LA PALABRA TENGA UN ESPACIO
    public boolean aceptacion(Coordenadas datos) {

        Iterator<Cola<Coordenadas>> it = this.getHojas();
        int tamaño = this.contarHojas();
        while (tamaño-- > 1) {
            Cola<Coordenadas> p = it.next().clonar();
            while (p.getTamanio() > 0) {
                if (p.deColar().equals(datos)) {
                    return false;
                }
            }

        }
        return true;
    }

    public int contarCantidadDeCh(String cadena) {
        int contador = 0;
        for (int i = 0; i < cadena.length(); i++) {
            if (cadena.charAt(i) == 'c') {
                if (i + 1 < cadena.length()) {
                    if (cadena.charAt(i + 1) == 'h') {
                        contador++;
                    }

                }

            }
        }

        return contador;
    }

    public int buscarPalabra(String cadena) {
        Cola<Coordenadas> vecCola[] = getVectorColaCoordenanadas(cadena);
        BuscadorDePalabrasEnArboles buscador = new BuscadorDePalabrasEnArboles(this,1, 0, this.contarCantidadDeCh(cadena), true);

        return buscador.buscarPalabra(cadena,vecCola);
    }
}
