package Modelo;

import java.util.Iterator;
import ufps.util.colecciones_seed.Cola;

public class BuscadorDePalabrasEnArboles {

    ArbolDigital arbolDeBusqueda;
    String cadena;
    int indice;
    int cantidadAparicionesPalabra;
    Coordenadas primerIndice;
    Coordenadas segundoIndice;
    Cola<Coordenadas> vecCola[];
    int cantidadDeCh;
    boolean empty;

    public BuscadorDePalabrasEnArboles(ArbolDigital arbolDeBusqueda, int indice, int contador, int cantidadDeCh, boolean empty) {
        this.arbolDeBusqueda = arbolDeBusqueda;
        this.indice = indice;
        this.cantidadAparicionesPalabra = contador;
        this.cantidadDeCh = cantidadDeCh;
        this.empty = empty;
    }

    public int getCantidadDeCh() {
        return cantidadDeCh;
    }

    public boolean vacio(Cola<Coordenadas> vec[]) {
        for (int i = 0; i < vec.length; i++) {

            Cola<Coordenadas> cola = (Cola) vec[i];

            if (cola == null) {

                return true;

            }
        }

        return false;
    }
//METODO PARA VERIFICAR QUE DESPUES DE LA CADENA VIENE UN ESPACIO EN BLANCO
    public boolean aceptacion(Coordenadas datos) {

        Iterator<Cola<Coordenadas>> it = arbolDeBusqueda.getHojas();
        int tamaño = arbolDeBusqueda.contarHojas();
        while (tamaño-- > 1) {
            Cola<Coordenadas> p = it.next().clonar();
            while (p.getTamanio() > 0) {
                if (p.deColar().equals(datos)) {
                    return false;
                }
            }

        }
        return true;
    }

    public void validarPalabra() {
        if (indice == cadena.length() - 1 - this.cantidadDeCh) {

            if (aceptacion(new Coordenadas(primerIndice.getX(), primerIndice.getY() + 1))) {
                cantidadAparicionesPalabra++;

            }
            actualizarPrimerIndice();

        } else {

            primerIndice = segundoIndice;
            vecCola[indice].deColar();
            indice++;
        }

    }

    public void actualizarPrimerIndice() {
        indice = 1;
        if (vecCola[0].getTamanio() > 0) {
            primerIndice = vecCola[0].deColar();
        } else {
            empty = false;
        }
    }

    public void actualizarSegundoIndice() {
        while (empty && (primerIndice.getY() >= vecCola[indice].getInfoInicio().getY() && primerIndice.getX() >= vecCola[indice].getInfoInicio().getX() || primerIndice.getX() > vecCola[indice].getInfoInicio().getX()) && vecCola[indice].getTamanio() > 0) {
            segundoIndice = vecCola[indice].deColar();
            if (!(vecCola[indice].getTamanio() > 0)) {
                empty = false;
            }
        }
        if (vecCola[indice].getTamanio() > 0) {
            segundoIndice = vecCola[indice].getInfoInicio();
        }
    }

    public boolean condicionAceptacion() {

        return ((primerIndice.getY() + 1 == segundoIndice.getY() && primerIndice.getX() == segundoIndice.getX()) || indice == cadena.length() - 1 - this.cantidadDeCh);
    }

    
    public boolean vecColaNoVacia() {
        return vecCola[indice].getTamanio() > 0 && vecCola[0].getTamanio() >= 0 && empty;
    }

    public boolean revisarPrimerIndiceAtras() {

        return primerIndice.getY() <= segundoIndice.getY() && primerIndice.getX() <= segundoIndice.getX();
    }

    public int buscarPalabra(String cadena,Cola<Coordenadas> vecCola[]) {
        this.setCadena(cadena);
        this.setVecCola(vecCola);
        primerIndice = this.vecCola[0].deColar();

        if (vacio(vecCola)) return 0;

        while (vecColaNoVacia()) {
            segundoIndice = vecCola[indice].getInfoInicio();

            if (condicionAceptacion()) {//si los indices estan seguidos
                validarPalabra();
            } else {
                if (revisarPrimerIndiceAtras()) {//si el primer indice esta atras
                    actualizarPrimerIndice();
                } else {
                    actualizarSegundoIndice();//si el segundo es el que esta atras
                    if (condicionAceptacion()) {
                        validarPalabra();
                    } else {
                        actualizarPrimerIndice();
                    }

                }
            }

        }
        return cantidadAparicionesPalabra;
    }

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }

    public int getCantidadAparicionesPalabra() {
        return cantidadAparicionesPalabra;
    }

    public void setCantidadAparicionesPalabra(int cantidadAparicionesPalabra) {
        this.cantidadAparicionesPalabra = cantidadAparicionesPalabra;
    }

    public Coordenadas getPrimerIndice() {
        return primerIndice;
    }

    public void setPrimerIndice(Coordenadas primerIndice) {
        this.primerIndice = primerIndice;
    }

    public Coordenadas getSegundoIndice() {
        return segundoIndice;
    }

    public void setSegundoIndice(Coordenadas segundoIndice) {
        this.segundoIndice = segundoIndice;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    public int getContador() {
        return cantidadAparicionesPalabra;
    }

    public void setContador(int contador) {
        this.cantidadAparicionesPalabra = contador;
    }

    public Coordenadas getPrimero() {
        return primerIndice;
    }

    public void setPrimero(Coordenadas primero) {
        this.primerIndice = primero;
    }

    public Coordenadas getSegundo() {
        return segundoIndice;
    }

    public void setSegundo(Coordenadas segundo) {
        this.segundoIndice = segundo;
    }

    public Cola<Coordenadas>[] getVecCola() {
        return vecCola;
    }

    public void setVecCola(Cola<Coordenadas>[] vecCola) {
        this.vecCola = vecCola;
    }

}
